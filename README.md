# div-01-Tests

#### Usage:

```console
student@ALEM-21f755:~/Desktop$ git clone https://git.01.alem.school/ituyakbayev/div-01-Tests.git
student@ALEM-21f755:~/Desktop$ cp -r div-01-Tests/* div-01/
student@ALEM-21f755:~/Desktop$ cd div-01/autotest/
student@ALEM-21f755:~/Desktop/div-01/autotest$ go test
```

#### Output:

```console
Atoi                 PASS (08/08)
RecursivePower       PASS (06/06)
PrintCombN           PASS (09/09)
PrintNbrBase         PASS (09/09)
Doop                 PASS (12/12)
Atoibase             PASS (09/09)
SplitWhiteSpaces     PASS (06/06)
Split                PASS (04/04)
ConvertBase          PASS (05/05)
RotateVowels         PASS (05/05)
AdvancedSortWordArr  PASS (06/06)
Cat                  PASS (06/06)
Ztail                PASS (05/05)
ActiveBits           PASS (07/07)
SortListInsert       PASS (05/05)
SortedListMerge      PASS (06/06)
ListRemoveIf         PASS (06/06)
BTreeTransplant      PASS (03/03)
BTreeApplyByLevel    PASS (02/02)
BTreeDeleteNode      PASS (04/04)

PASS
ok      _/home/student/Desktop/div-01/autotest      0.041s
```
